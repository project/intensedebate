<?php

function intensedebate_admin_settings() {
  $form['id_disabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Disable IntenseDebate comments?'),
      '#default_value' => variable_get('intensedebate_disabled', TRUE),
      '#description' => t('Prevent IntenseDebate comments from being displayed.'),
      );

  $form['acct_num'] = array(
      '#type' => 'textfield',
      '#title' => t('IntenseDebate account number'),
      '#default_value' => variable_get('intensedebate_acct', ''),
      '#description' => t('This is the IntenseDebate account number, unique to each station.'),
      );

  $form['test_mode'] = array(
      '#type'           => 'checkbox',
      '#title'          => t('Test Mode'),
      '#default_value'  => variable_get('intensedebate_testmode', FALSE),
      '#description'    => t('When testing, check this so test comments will not show up in production.'),

      );

  $form['node_types'] = array(
      '#type' => 'select',
      '#options' => node_get_types('names'),
      '#default_value' => variable_get('intensedebate_node_types', array()),
      '#title' => t('Node Types'),
      '#description' => t('Display comments only on the selected node types'),
      '#multiple' => TRUE,
      );

  $form['location'] = array(
      '#type' => 'select',
      '#title' => t('Location'),
      '#description' => t('Display the IntenseDebate comments in the given location. When "Block" is selected, the comments will appear in the <a href="@intensedebate">IntenseDebate Comments block</a>.', array('@intensedebate' => url('admin/build/block'))),
      '#default_value' => variable_get('intensedebate_location', 'content_area'),
      '#options' => array(
        'content_area' => t('Content Area'),
        'block' => t('Block'),
        ),
      );

  $form['weight'] = array(
      '#type' => 'select',
      '#title' => t('Weight'),
      '#description' => t('When the comments are displayed in the content area, you can change the position at which they will be shown.'),
      '#default_value' => variable_get('intensedebate_weight', 50),
      '#options' => drupal_map_assoc(array(-100, -50, -25, 0, 25, 50, 100)),
      );

  $form['path_prefix'] = array(
      '#type' => 'textfield',
      '#size' => 15,
      '#title' => t('Path Prefix'),
      '#description' => t("Prepend this value to the post_id used by IntenseDebate.  If you don't know what this is, don't worry about it."),
      '#default_value' => variable_get('intensedebate_path_prefix', ''),
      );


  $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
      );
  return $form;
}

function intensedebate_admin_settings_validate(&$form_state, $form) {
    //check that the api key looks ok (is this documented somewhere?)
    //try to establish a connection with intensedebate to make sure it's valid
}

function intensedebate_admin_settings_submit(&$form_state, $form) {
  drupal_set_message(t("Your changes have been saved."));
  foreach ($form_state['node_types']['#value'] as $key => $value) {
    $id_node_types[] = $key;
  }
  variable_set('intensedebate_node_types', $id_node_types);
  variable_set('intensedebate_acct', $form_state['acct_num']['#value']);
  variable_set('intensedebate_disabled', $form_state['id_disabled']['#value']);
  variable_set('intensedebate_testmode', $form_state['test_mode']['#value']);
  variable_set('intensedebate_weight', $form_state['weight']['#value']);
  variable_set('intensedebate_location', $form_state['location']['#value']);
  variable_set('intensedebate_path_prefix', $form_state['path_prefix']['#value']);
}

